from flask import Flask, render_template, flash, redirect, url_for, session, logging, request
from data import Products, Biokons
# from produk import Products
# from flask_mysqldb import MySQL
from flaskext.mysql import MySQL
from pymysql.cursors import DictCursor
from flask_googlemaps import GoogleMaps
from flask_googlemaps import Map, icons

from wtforms import Form, StringField, TextAreaField, PasswordField, SelectField, validators
# from passlib.hash import sha256_crypt
from functools import wraps

import sys

app = Flask(__name__)

mysql = MySQL(cursorclass=DictCursor)

#config MySQL
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = '123'
app.config['MYSQL_DATABASE_DB'] = 'biokonversi'
# app.config['MYSQL_DATABASE_CURSORCLASS'] = 'DictCursor'

# googlemaps config
app.config['GOOGLEMAPS_KEY'] = "AIzaSyCsC7xyIEr_nOZzitGARoVgpAyMeN9WoqQ"

# Init google maps
GoogleMaps(app, key="AIzaSyCsC7xyIEr_nOZzitGARoVgpAyMeN9WoqQ")

#init MySQL
mysql.init_app(app)

# Articles = Articles()

Products = Products()

Biokons = Biokons()

# index
@app.route('/')
def index():
  # create cursor
  cur = mysql.get_db().cursor()

  # get articles
  result = cur.execute("SELECT * FROM articles WHERE stat='post' ORDER BY id DESC LIMIT 2")

  articles = cur.fetchall()

  # get testimoni
  result = cur.execute("SELECT * FROM testimonis WHERE stat='post' ORDER BY id DESC LIMIT 2")

  testimonis = cur.fetchall()

  # get kegiatans
  result = cur.execute("SELECT * FROM kegiatans WHERE stat='post' ORDER BY id DESC LIMIT 2")

  kegiatans = cur.fetchall()

  if result > 0:
    return render_template('index.html', articles=articles, testimonis=testimonis, kegiatans=kegiatans)
  else:
    msg='NO ARTICLES FOUND'
    return render_template('index.html', msg=msg)

  #close connection
  cur.close()
  # return render_template('index.html', articles = Articles)

# produk
@app.route('/produk')
def produk():
  return render_template('produk.html')

@app.route('/produk_biokonversi')
def produk_biokonversi():
  return render_template('produk_biokonversi.html', products = Products)

@app.route('/produk_biokonversi_detail_1')
def produk_biokonversi_detail_1():
  return render_template('produk_biokonversi_detail_1.html')

@app.route('/produk_biokonversi_detail_2')
def produk_biokonversi_detail_2():
  return render_template('produk_biokonversi_detail_2.html')

@app.route('/produk_biokonversi_detail_3')
def produk_biokonversi_detail_3():
  return render_template('produk_biokonversi_detail_3.html')

@app.route('/produk_biokon')
def produk_biokon():
  return render_template('produk_biokon.html', biokons = Biokons)

@app.route('/produk_biokon_detail_1')
def produk_biokon_detail_1():
  return render_template('produk_biokon_detail_1.html')

@app.route('/produk_biokon_detail_2')
def produk_biokon_detail_2():
  return render_template('produk_biokon_detail_2.html')

@app.route('/produk_biokon_detail_3')
def produk_biokon_detail_3():
  return render_template('produk_biokon_detail_3.html')

@app.route('/produk_biokon_detail_4')
def produk_biokon_detail_4():
  return render_template('produk_biokon_detail_4.html')

@app.route('/produk_biokon_detail_5')
def produk_biokon_detail_5():
  return render_template('produk_biokon_detail_5.html')


@app.route('/tentang')
def tentang():
  return render_template('tentang.html')


@app.route('/distribusi')
def distribusi():
  distributionmap = Map(
        identifier="distributionmap",
        varname="distributionmap",
        lat=-6.25,
        lng=106.83,
        maptype_control=False,
        # zoom=6,
        style=(
            "height:500px;"
            "width:100%;"
            "box-shadow: 0px 5px 15px;"
        ),
        scroll_wheel=False,
        # zoom_control=False,
        streetview_control=False,
        markers=[
            {
                # 'icon': icons.alpha.B,
                'lat': -6.253787, 
                'lng': 106.833527,
                'infobox': (
                    "<h6><strong>Toko Hobinosco</strong></h6>"
                    "<p>Jl. Raya Cilangkap No.15, RT.1/RW.5,<br>Cipayung, Kota Jakarta Timur,<br>Daerah Khusus Ibukota Jakarta 13870</p>"
                    "<a href='https://www.tokopedia.com/hobinosco' class='btn btn-tokped mx-1' target='_blank'>Tokopedia</a>"
                    "<a href='https://www.bukalapak.com/u/hobinosco' class='btn btn-buka mx-1' target='_blank'>Bukalapak</a>"
                    "<a href='http://shopee.co.id/hobinosco' class='btn btn-shopee mx-1' target='_blank'>Shopee</a>"
                )
            },
            {
                # 'icon': icons.dots.blue,
                'lat': -6.257317, 
                'lng': 106.819891,
                'infobox': (
                    "<h6><strong>Toko Hobinosco</strong></h6>"
                    "<p>Jl. Raya Cilangkap No.15, RT.1/RW.5,<br>Cipayung, Kota Jakarta Timur,<br>Daerah Khusus Ibukota Jakarta 13870</p>"
                    "<a href='https://www.tokopedia.com/hobinosco' class='btn btn-tokped mx-1' target='_blank'>Tokopedia</a>"
                    "<a href='https://www.bukalapak.com/u/hobinosco' class='btn btn-buka mx-1' target='_blank'>Bukalapak</a>"
                    "<a href='http://shopee.co.id/hobinosco' class='btn btn-shopee mx-1' target='_blank'>Shopee</a>"
                )
            },
            {
                # 'icon': '//maps.google.com/mapfiles/ms/icons/yellow-dot.png',
                'lat': -6.238729, 
                'lng': 106.812288,
                'infobox': (
                    "<h6><strong>Toko Hobinosco</strong></h6>"
                    "<p>Jl. Raya Cilangkap No.15, RT.1/RW.5,<br>Cipayung, Kota Jakarta Timur,<br>Daerah Khusus Ibukota Jakarta 13870</p>"
                    "<a href='https://www.tokopedia.com/hobinosco' class='btn btn-tokped mx-1' target='_blank'>Tokopedia</a>"
                    "<a href='https://www.bukalapak.com/u/hobinosco' class='btn btn-buka mx-1' target='_blank'>Bukalapak</a>"
                    "<a href='http://shopee.co.id/hobinosco' class='btn btn-shopee mx-1' target='_blank'>Shopee</a>"
                )
            }
        ]
    )
  return render_template('distribusi.html', distributionmap = distributionmap)


@app.route('/dokumen')
def dokumen():
  return render_template('dokumen.html')

@app.route('/dokumen_isi/<string:id>/')
def dokumen_isi(id):
   # create cursor
  cur = mysql.get_db().cursor()

  # get article
  result = cur.execute("SELECT * FROM articles WHERE id = %s AND stat!='del'", [id])

  article = cur.fetchone()

  return render_template('dokumen_isi.html', article = article)

@app.route('/testimoni_isi/<string:id>/')
def testimoni_isi(id):
   # create cursor
  cur = mysql.get_db().cursor()

  # get testimoni
  result = cur.execute("SELECT * FROM testimonis WHERE id = %s AND stat!='del'", [id])

  testimoni = cur.fetchone()

  return render_template('testimoni_isi.html', testimoni = testimoni)

@app.route('/kegiatan_isi/<string:id>/')
def kegiatan_isi(id):
   # create cursor
  cur = mysql.get_db().cursor()

  # get kegiatan
  result = cur.execute("SELECT * FROM kegiatans WHERE id = %s AND stat!='del'", [id])

  kegiatan = cur.fetchone()

  return render_template('kegiatan_isi.html', kegiatan = kegiatan)

@app.route('/media')
def media():
  return render_template('media.html')

@app.route('/kontak')
def kontak():
  return render_template('kontak.html')

# Admin Login Panel
@app.route('/login', methods=['GET', 'POST'])
def login():
  if request.method == 'POST':
    # Get Form Fields
    username = request.form['username']
    password_candidate = request.form['password']

    # Create Cursor
    cur = mysql.get_db().cursor()

    # Get User by Username
    result = cur.execute("SELECT * FROM user WHERE username = %s", [username])

    if result > 0:
      # get stored hash
      data = cur.fetchone()
      password = data['password']

      # compare the password
      if password == password_candidate:
        # Password Matched
        session['logged_in'] = True
        session['username'] = username
        return redirect(url_for('admin'))

      else:
        error = 'Invalid Passowrd'
        return render_template('login.html', error=error)

      #close connection
      cur.close()

    else:
      error = 'Username not found'
      return render_template('login.html', error=error)

  return render_template('login.html')

# check if user logged in
def is_logged_in(f):
  @wraps(f)
  def wrap(*args, **kwargs):
    if 'logged_in' in session:
      return f(*args, **kwargs)
    else:
      return redirect(url_for('login'))
  return wrap

@app.route('/logout')
def logout():
  session.clear()
  return redirect(url_for('login'))

@app.route('/admin')
@is_logged_in
def admin():
  # create cursor
  cur = mysql.get_db().cursor()

  # get articles
  result = cur.execute("SELECT * FROM articles WHERE stat!='del'")

  articles = cur.fetchall()

  if result >0:
    return render_template('admin.html', articles=articles)
  else:
    msg='NO ARTICLES FOUND'
    return render_template('admin.html', msg=msg)

  #close connection
  cur.close()

# Article Form Class
class ArticleForm(Form):
  title = StringField('Title', [validators.length(min=1, max=500)])
  body = TextAreaField('Body', [validators.length(min=10)])
  img = StringField('Img', [validators.length(min=1, max=100)])
  stat = SelectField('Stat', choices=[('arch', 'archive'), ('post', 'post')])
  
@app.route('/buat_berita', methods=['GET', 'POST'])
@is_logged_in
def buat_berita():
  form = ArticleForm(request.form)
  if request.method == 'POST' and form.validate():
    title = form.title.data
    body = form.body.data
    img = form.img.data
    stat = form.stat.data

    # Create Cursor
    cur = mysql.get_db().cursor()

    # Execute
    cur.execute('INSERT INTO articles(title, body, img, stat) VALUES(%s, %s, %s, %s)', (title, body, img, stat))

    # Commit to DB
    mysql.get_db().commit()

    # Close Connection
    cur.close()

    return redirect(url_for('admin'))

  return render_template('buat_berita.html', form=form)

@app.route('/edit_berita/<string:id>', methods=['GET', 'POST'])
@is_logged_in
def edit_berita(id):
  # Create Cursor
  cur = mysql.get_db().cursor()

  # Execute
  result = cur.execute("SELECT * FROM articles WHERE id = %s", [id])

  article = cur.fetchone()

  # get form
  form = ArticleForm(request.form)

  # populate article form fields
  form.title.data = article['title']
  form.body.data = article['body']
  form.img.data = article['img']
  form.stat.data = article['stat']

  if request.method == 'POST' and form.validate():
    title = request.form['title']
    body = request.form['body']
    img = request.form['img']
    stat = request.form['stat']

    # Create Cursor
    cur = mysql.get_db().cursor()

    # Execute
    cur.execute('UPDATE articles SET title=%s, body=%s, img=%s, stat=%s WHERE id=%s', (title, body, img, stat, id))

    # Commit to DB
    mysql.get_db().commit()

    # Close Connection
    cur.close()

    return redirect(url_for('admin'))

  return render_template('edit_berita.html', form=form)

# TESTIMONI
@app.route('/view_testimoni')
@is_logged_in
def view_testimoni():
  # create cursor
  cur = mysql.get_db().cursor()

  # get articles
  result = cur.execute("SELECT * FROM testimonis WHERE stat!='del'")

  testimonis = cur.fetchall()

  if result >0:
    return render_template('view_testimoni.html', testimonis=testimonis)
  else:
    msg='NO ARTICLES FOUND'
    return render_template('view_testimoni.html', msg=msg)

  #close connection
  cur.close()

# Testimoni Form Class
class TestimoniForm(Form):
  title = StringField('Title', [validators.length(min=1, max=500)])
  body = TextAreaField('Body', [validators.length(min=10)])
  img = StringField('Img', [validators.length(min=1, max=100)])
  stat = SelectField('Stat', choices=[('arch', 'archive'), ('post', 'post')])
  
@app.route('/buat_testimoni', methods=['GET', 'POST'])
@is_logged_in
def buat_testimoni():
  form = TestimoniForm(request.form)
  if request.method == 'POST' and form.validate():
    title = form.title.data
    body = form.body.data
    img = form.img.data
    stat = form.stat.data

    # Create Cursor
    cur = mysql.get_db().cursor()

    # Execute
    cur.execute('INSERT INTO testimonis(title, body, img, stat) VALUES(%s, %s, %s, %s)', (title, body, img, stat))

    # Commit to DB
    mysql.get_db().commit()

    # Close Connection
    cur.close()

    return redirect(url_for('view_testimoni'))

  return render_template('buat_testimoni.html', form=form)

@app.route('/edit_testimoni/<string:id>', methods=['GET', 'POST'])
@is_logged_in
def edit_testimoni(id):
  # Create Cursor
  cur = mysql.get_db().cursor()

  # Execute
  result = cur.execute("SELECT * FROM testimonis WHERE id = %s", [id])

  testimoni = cur.fetchone()

  # get form
  form = TestimoniForm(request.form)

  # populate article form fields
  form.title.data = testimoni['title']
  form.body.data = testimoni['body']
  form.img.data = testimoni['img']
  form.stat.data = testimoni['stat']

  if request.method == 'POST' and form.validate():
    title = request.form['title']
    body = request.form['body']
    img = request.form['img']
    stat = request.form['stat']

    # Create Cursor
    cur = mysql.get_db().cursor()

    # Execute
    cur.execute('UPDATE testimonis SET title=%s, body=%s, img=%s, stat=%s WHERE id=%s', (title, body, img, stat, id))

    # Commit to DB
    mysql.get_db().commit()

    # Close Connection
    cur.close()

    return redirect(url_for('view_testimoni'))

  return render_template('edit_testimoni.html', form=form)


# kegiatan
@app.route('/view_kegiatan')
@is_logged_in
def view_kegiatan():
  # create cursor
  cur = mysql.get_db().cursor()

  # get articles
  result = cur.execute("SELECT * FROM kegiatans WHERE stat!='del'")

  kegiatans = cur.fetchall()

  if result >0:
    return render_template('view_kegiatan.html', kegiatans=kegiatans)
  else:
    msg='NO ARTICLES FOUND'
    return render_template('view_kegiatan.html', msg=msg)

  #close connection
  cur.close()

# kegiatan Form Class
class kegiatanForm(Form):
  title = StringField('Title', [validators.length(min=1, max=500)])
  body = TextAreaField('Body', [validators.length(min=10)])
  img = StringField('Img', [validators.length(min=1, max=100)])
  stat = SelectField('Stat', choices=[('arch', 'archive'), ('post', 'post')])
  
@app.route('/buat_kegiatan', methods=['GET', 'POST'])
@is_logged_in
def buat_kegiatan():
  form = kegiatanForm(request.form)
  if request.method == 'POST' and form.validate():
    title = form.title.data
    body = form.body.data
    img = form.img.data
    stat = form.stat.data

    # Create Cursor
    cur = mysql.get_db().cursor()

    # Execute
    cur.execute('INSERT INTO kegiatans(title, body, img, stat) VALUES(%s, %s, %s, %s)', (title, body, img, stat))

    # Commit to DB
    mysql.get_db().commit()

    # Close Connection
    cur.close()

    return redirect(url_for('view_kegiatan'))

  return render_template('buat_kegiatan.html', form=form)

@app.route('/edit_kegiatan/<string:id>', methods=['GET', 'POST'])
@is_logged_in
def edit_kegiatan(id):
  # Create Cursor
  cur = mysql.get_db().cursor()

  # Execute
  result = cur.execute("SELECT * FROM kegiatans WHERE id = %s", [id])

  kegiatan = cur.fetchone()

  # get form
  form = kegiatanForm(request.form)

  # populate article form fields
  form.title.data = kegiatan['title']
  form.body.data = kegiatan['body']
  form.img.data = kegiatan['img']
  form.stat.data = kegiatan['stat']

  if request.method == 'POST' and form.validate():
    title = request.form['title']
    body = request.form['body']
    img = request.form['img']
    stat = request.form['stat']

    # Create Cursor
    cur = mysql.get_db().cursor()

    # Execute
    cur.execute('UPDATE kegiatans SET title=%s, body=%s, img=%s, stat=%s WHERE id=%s', (title, body, img, stat, id))

    # Commit to DB
    mysql.get_db().commit()

    # Close Connection
    cur.close()

    return redirect(url_for('view_kegiatan'))

  return render_template('edit_kegiatan.html', form=form)


if __name__ == '__main__':
  app.secret_key = 'BIOKON2018'
  app.run(debug=True)
