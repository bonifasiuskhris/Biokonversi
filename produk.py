def Products():
  products = [
    {
      'id': 1,
      'title' : 'Pupuk Cair Biokonversi',
      'content' : '1 Liter',
      'desc': 'Mengembalikan kesuburan tanah,  dengan memperbaiki kondisi  biologi, fisik dan kimia tanah,  sehingga dapat menyediakan  sumber hara bagi tanaman',
      'link' : '/produk_biokonversi_detail_1',
      'img1' : 'p-bio-1.jpeg',
    },    {
      'id': 2,
      'title' : 'Pupuk Cair Biokonversi',
      'content' : '10 Liter',
      'desc': 'Meningkatkan kemampuan daun menarik  unsur hara dalam proses transportasi unsur  hara dari akar dan merangsang pertumbuhan  akar, batang, daun agar berkembang  sempurna serta memperpanjang usia akar',
      'link' : '/produk_biokonversi_detail_2',
      'img1' : 'p-bio-2.jpeg',
    },    {
      'id': 3,
      'title' : 'Pupuk Cair Biokonversi',
      'content' : '20 Liter',
      'desc': 'Meningkatkan daya tahan tanaman,  terutama melindungi akar terhadap  serangan hama dan penyakit',
      'link' : '/produk_biokonversi_detail_3',
      'img1' : 'p-bio-3.jpeg',
    },
  ]

  return products